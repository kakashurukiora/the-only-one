[space]
[container]
[row]
[column width="8"]
#Documentation : structure
## Qu'est-ce c'est ?
Pour bien fonctionner, Kohkiri met à disposition différents éléments qui vont vous permettre de bien structurer votre site internet.

Dans cette partie, nous allons aussi mettre en avant différents termes qui seront largement utilisé dans le reste de cette documentation.

## Les entités
Le premier groupe d'éléments qui composent la structure du site sont les entités et peuvent avoir un type différent (page, game, soluce etc.)
Nous allons voir comment les déclarer et comment les utiliser.
### Déclarer une entité
Pour pouvoir utiliser une entité, vous devez d'abord la déclarer dans le dossier structure. Pour cela, il faut créer un fichier JSON dont le nom doit être obligatoirement le nom de l'entité.

**Exemple :** Pour déclarer l'entité game, vous devez créer un fichier game.json dans le dossier structure.

Une fois le fichier créé, vous allez pouvoir définir chaque propriété de l'entité (name, creation_date, slug etc.).

**Exemple :**
`
{
  "id": {
    "type": "string",
    "label": "Id",
    "required": true
  }
}  
`

**Remarques :**
* Vous pouvez ajouter autant de propriétés que vous voulez mais certaines sont obligatoires et doivent donc exister dans la structure.
* Si une propriété obligatoire manque dans la structure, l'entité concernée ne sera jamais appelée par Kohkiri.

### Contribuer une entité
Maintenant que vous avez déclaré un type d'entité, vous pouvez en contribuer autant que vous le souhaitez dans le répertoire entities. Vous pouvez organiser ce répertoire comme vous le souhaitez et créer autant de sous-répertoires que vous voulez. La seule règle que vous devez avoir en tête, c'est que contribuer une entité c'est ajouter deux fichiers : Un fichier JSON et un fichier MARKDOWN.
Les deux fichiers doivent avoir exactement le même nom.

Exemple : `the-legend-of-zelda-mystery-of-solarus-dx.json` et `the-legend-of-zelda-mystery-of-solarus-dx.md`
#### Le fichier JSON
Le fichier JSON sert à spécifier toutes les valeurs des propriétés de l'entité (name, creation_date, slug etc.). Certaines sont obligatoires et d'autres non. Toutes ces propriétés ont du être déclarés dans la structure du site.
#### Le fichier MARKDOWN
Le fichier Markdown sert à contribuer le contenu de l'entité. Vous avez la possibilité d'écrire en markdown à l'intérieur mais aussi d'utiliser d'autres choses que nous expliquerons ultérieurement.
## Les pages
Les pages sont des entités spéciales et permettent de créer des points d'entrées sur le site. C'est en effet grâce à elle que nous allons pouvoir créer de nouveaux permaliens sur le site.
Un permalien est une chaine unique. Il est contenu dans une url et est situé juste après le domaine et la langue du site.

** Exemple : **
`https://www.domaine.com/en/votre-permalien`

Une page donne toujours accès à un seul et unique permalien. Quand vous appelez une url valide du site, Kohkiri va tenter de trouver la bonne entité de type page à appeler et va tenter de l'afficher.
Si l'entité n'est pas trouvée, le site affichera automatiquement une erreur 404.
Vous avez bien entendu la possibilité de configurer votre page en modifiant ses propriétés.

Enfin, comme tout entité qui se repsecte, vous avez aussi la possibilité d'étendre les propriétés des pages comme bon vous semble en modifiant sa structure.

## Le markdown

Le mode de contribution principal de Kohkiri est le markdown.

## Les shortcodes

### Présentation
En plus du markdown, vous avez la possibilité d'utiliser des shortcodes. Il s'agit de chaines de caractères spéciales qui vont permettre d'afficher des blocs avancés sur vos pages.
Il existe pas mal de shortcodes.
### Utilisation
Un shortcode est toujours composé d'une balise ouvrante et du'une balise fermante. De plus, nous pouvons auusi ajouter des paramètres à la balise ouvrante afin de modifier l'affichage du shortcode.
Exemple : `[mon-shortcode param="1" param="2"]Contenu[/mon-shortcode]`

### Quelques remarques...

* Il est tout à fait possible de combiner les shortcodes avec du markdown.
* De plus, vous pouvez imbriquer les shortcodes entre eux et cela de manière illimité.

## Les modèles
Les modèles sont tout simplemnt des shortcodes custom que vous pouvez appeler n'importe où sur vos pages.
Il est caractérisé par un id
## Les variables

### Présentation
Enfin, vous avez la possibilité d'utiliser des variables dans vos pages qui sont directement reliés aux propriété de la page ou de l'entité en cours.

### Utilisation
Une variable commence toujours par une accolade ouvrante et se fini toujours par une accolade fermante.
Le nom de la variable doit forcément être le nom d'une propriété de l'entité courante (title, slug etc.). Cette dernière est généralement une page.
Exemple : `{ma-variable}`
[/column]
[column width="4"]
[summary][/summary]
[/column]
[/row]
[/container]

