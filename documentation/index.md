[space]
[container]
[row]
[column width="8"]
#Documentation
## Préambule
Cette documentation a pour objectif d'expliquer le fonctionnement complet de Kohkiri et de son système de gestion de contenu.
Avec Kohkiri, vous allez apprendre à structurer et contribuer votre site internet de A à Z.
## Technologies utilisées
* PHP 7
* Kohseven
* jQuery
* Bootstrap 4
* Fontawesome 5

## Prérequis
Pour utiliser Kohkiri, vous avez besoin de Apache ou Nginx et de PHP 7.
[/column]
[column width="4"]
[summary][/summary]
[/column]
[/row]
[/container]

