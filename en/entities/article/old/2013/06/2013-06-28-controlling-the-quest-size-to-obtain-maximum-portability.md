Many new features are planned for Solarus 1.1. I will talk about the important ones whenever I implement them.

The one I have been working on for the last few days is an important improvement for quest makers, for players and also for people who compile the engine on various systems.

In Solarus 1.0, the size of the visible space that appears on the screen (we call this the "quest size") is a constant defined at compile time: by default, 320x240. This "quest size" is how much content of the map you can see when playing. It should not be confused with the video mode of the screen: the video mode just defines how this 320x240 quest space is rendered on the screen: directly in a window, stretched or scaled to 640x480, in fullscreen or not, etc.

The main problem of this approach is that everything is decided at compilation time. As the creator of a quest, you have no control over the screen size and your game is just supposed to adapt itself with any size. Which is just not possible. You don't want a game area of 1000x1000 because the player could see way too much content of your maps! Still, you may want to allow different sizes (like 400x240) so that the game fills the entire space of wide screens, using square pixels and�without vertical�black bars.

To solve the problem, in Solarus 1.1, you will be able to set the range of sizes that your quest allows.

If you are okay to allow people with a wide screen to see more content that others, you can set a range of sizes in your quest properties file (quest.dat):
<pre>quest{
 solarus_version = "1.1",
 write_dir = "my_quest",
 title_bar = "My incredible quest",
 normal_quest_size = "320x240", -- Works on most configurations.
 min_quest_size = "320x200",    -- Might make fullscreen compatible with more systems.
 max_quest_size = "400x240",    -- Suited for wide screens, including mobile devices like Pandora.
}</pre>
Of course, your maps and your menus will have to adapt correctly to any value inside this range. So it requires more work, but your game will be more portable. There is a new Lua function sol.video.get_quest_size() to help you with that.

At runtime, the user can choose�his quest size with a command-line option. A default quest size can also be set at compilation time. If the size requested is not in the range allowed by your quest, then normal_quest_size is used instead.

Then, Solarus does the complicated work for you :). It automatically�detects the appropriate resolutions to render this size�at best for each video mode (fullscreen, with scaling, etc.). So you don't have to worry about that part. Besides, the Lua API of video modes does not change at all.

If you prefer that all people play your quest with the exact same visible area, set all three sizes to the same value, or just define normal_quest_size (by default, min and max take the normal value):
<pre>quest{
 solarus_version = "1.1",
 write_dir = "my_quest",
 title_bar = "My incredible quest",
 normal_quest_size = "320x240", -- Only one allowed quest size.
}</pre>