The new version of Solarus is available now!

The main change of Solarus 1.1 is that there are no more hardcoded menus in the engine: the dialog box and the game-over menu are now fully scripted in your quest with the Lua API. There are a lot of other new features and improvements, as well as bug fixes, so it is recommended to upgrade.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/downloads/download-solarus/">Download Solarus 1.1</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/solarus/quest-editor/">Solarus Quest Editor</a></li>
	<li><a href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
</ul>
Our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games. ZSDX is now available in simplified Chinese and traditional Chinese (beta). Thanks Sundae and Rypervenche! Note that a Spanish version of ZSXD is also in progress (thanks Xexio!).
<ul>
	<li><a title="Download ZSDX" href="http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/">Download Zelda Mystery of Solarus DX 1.7</a></li>
	<li><a title="Download ZSXD" href="http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/">Download Zelda Mystery of Solarus XD 1.7</a></li>
</ul>
Solarus 1.1 also improves portability and performance. This allows us to release the Android and OpenPandora versions of our games. They will be available in the next few days!

Here is the full changelog. 80 github issues were resolved for Solarus 1.1 and 20 more for ZSDX and ZSXD 1.7 so this is a huge list!
<h2>Changes in Solarus 1.1</h2>
New features:
<ul>
	<li>Add a very short <strong>sample quest</strong> with free graphics and musics (#232, #318).</li>
	<li>Allow <strong>scripted dialog boxes</strong> (#184).</li>
	<li>Allow a <strong>scripted game-over menu</strong> (#261).</li>
	<li>Replace the old built-in dialog box by a very minimal one.</li>
	<li>Remove the old built-in game-over menu.</li>
	<li>Remove the old built-in dark rooms displaying (#205).</li>
	<li>New entity: <strong>separators</strong> to visually separate some regions in a map (#177).</li>
	<li>New type of ground: <strong>ice</strong> (#182).</li>
	<li>New type of ground: <strong>low walls</strong> (#117).</li>
	<li>Blocks and thrown items <strong>can now fall into holes, lava and water</strong> (#191).</li>
	<li>Kill <strong>enemies that fall into holes, lava and water</strong> (#190).</li>
	<li>Allow quest makers and users to set the size of the playing area.</li>
	<li>Allow maps to have a <strong>default destination entity</strong> (#231).</li>
	<li>A game can now start without specifying an initial map and destination.</li>
	<li>Stairs inside a single floor can now go from any layer to a next one (#178).</li>
	<li>The channel volume of  .it musics can now be changed dynamically (#250).</li>
	<li>The tempo of  .it musics can now be changed dynamically (#250).</li>
	<li>Allow Lua to manipulate files directly (#267).</li>
	<li>New syntax of <strong>sprites</strong>, easier to read and parse (#168).</li>
	<li>New syntax of <strong>project_db.dat</strong>, easier to read and parse (#169).</li>
	<li><strong>languages.dat</strong> no longer exists. Languages are in project_db.dat now (#265).</li>
	<li>The quest archive can now also be named data.solarus.zip (#293).</li>
</ul>
Lua API changes that introduce incompatibilities (see the <a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">migration guide</a>):
<ul>
	<li>map:is_dialog_enabled() is replaced by game:is_dialog_enabled().</li>
	<li>map:start_dialog() is replaced by game:start_dialog().</li>
	<li>Remove map:draw_dialog_box(), no longer needed.</li>
	<li>Remove map:set_dialog_style(): replace it in your own dialog box system.</li>
	<li>Remove map:set_dialog_position(): replace it in your own dialog box system.</li>
	<li>Remove map:set_dialog_variable(): use the info param of game:start_dialog().</li>
	<li>Make map:get_entities() returns an iterator instead of an array (#249).</li>
	<li>Replace map:set_pause_enabled() by game:set_pause_allowed().</li>
	<li>Make the enemy:create_enemy() more like map:create_enemy() (#215).</li>
	<li>Remove sol.language.get_default_language(), useless and misleading (#265).</li>
	<li>Remove sol.main.is_debug_enabled().</li>
	<li>Remove map:get_light() and map:set_light() (#205).</li>
	<li>In game:get/set_ability(), ability "get_back_from_death" no longer exists.</li>
	<li>Empty chests no longer show a dialog if there is no on:empty() event (#274).</li>
</ul>
Lua API changes that do not introduce incompatibilities:
<ul>
	<li>game:get/set_starting_location(): map and destination can now be nil.</li>
	<li>hero:teleport(): make destination optional (maps now have a default one).</li>
	<li>map:create_teletransporter(): make destination optional.</li>
	<li>Add a function sol.video.get_quest_size().</li>
	<li>Make map:get_camera_position() also return the size of the visible area.</li>
	<li>Add a method entity:is_in_same_region(entity).</li>
	<li>Add a method entity:get_center_position().</li>
	<li>Add methods entity:get_direction4_to(), entity:get_direction8_to() (#150).</li>
	<li>Add a method game:get_hero().</li>
	<li>Add methods hero:get/set_walking_speed() (#206).</li>
	<li>Add hero:get_state() and hero:on_state_changed() (#207).</li>
	<li>Add events separator:on_activating() and separator:on_activated() (#272).</li>
	<li>Add methods enemy:is/set_traversable() (#147).</li>
	<li>Add a method enemy:immobilize() (#160).</li>
	<li>Add on_position_changed() to all entities, not only enemies (#298).</li>
	<li>Add on_obstacle_reached() to all entities, not only enemies (#298).</li>
	<li>Add on_movement_changed() to all entities, not only enemies (#298).</li>
	<li>Add on_movement_finished() to all entities, not only enemies/NPCs (#298).</li>
	<li>target_movement:set_target(entity) now accepts an x,y offset (#154).</li>
	<li>Add a method game:is_pause_allowed().</li>
	<li>Add a method map:get_ground() (#141).</li>
	<li>Add a method map:get_music() (#306).</li>
	<li>Add an optional parameter on_top to sol.menu.start.</li>
	<li>Add sprite:on_animation_changed() and sprite:on_direction_changed() (#153).</li>
	<li>Add a function sol.input.is_key_pressed().</li>
	<li>Add a function sol.input.is_joypad_button_pressed().</li>
	<li>Add a function sol.input.get_joypad_axis_state().</li>
	<li>Add a function sol.input.get_joypad_hat_direction().</li>
	<li>Add functions sol.input.is/set_joypad_enabled() (#175).</li>
	<li>Add a function sol.audio.get_music() (#146).</li>
	<li>Add a function sol.audio.get_music_format().</li>
	<li>Add a function sol.audio.get_music_num_channels().</li>
	<li>Add functions sol.audio.get/set_music_channel_volume() for .it files (#250).</li>
	<li>Add functions sol.audio.get/set_music_tempo() for .it files (#250).</li>
	<li>Return nil if the string is not found in sol.language.get_string().</li>
	<li>sol.language.get_dialog() is now implemented.</li>
	<li>Add a function game:stop_dialog(status) to close the scripted dialog box.</li>
	<li>Add an event game:on_dialog_started(dialog, info).</li>
	<li>Add an event game:on_dialog_finished(dialog).</li>
	<li>Add functions game:start_game_over() and game:stop_game_over (#261).</li>
	<li>Add events game:on_game_over_started(), game:on_game_over_finished (#261).</li>
	<li>Add sol.file functions: open(), exists(), remove(), mkdir() (#267).</li>
</ul>
Bug fixes:
<ul>
	<li>Fix map menus not receiving on_command_pressed/released() events.</li>
	<li>Fix camera callbacks never called when already on the target (#308).</li>
	<li>Fix a crash when adding a new menu during a menu:on_finished() event.</li>
	<li>Fix a crash when calling hero:start_victory() without sword.</li>
	<li>Fix an error when loading sounds (#236). Sounds were working anyway.</li>
	<li>Fix a possible memory error when playing sounds.</li>
	<li>Fix blocks that continue to follow the hero after picking a treasure (#284).</li>
	<li>Fix on_obtained() that was not called for non-brandished treasures (#295).</li>
	<li>Jumpers can no longer be activated the opposite way when in water.</li>
	<li>Jumpers are now activated after a slight delay (#253).</li>
	<li>Sensors no longer automatically reset the hero's movement (#292).</li>
	<li>Correctly detect the ground below the hero or any point.</li>
	<li>Don't die if there is a syntax error in dialogs.dat.</li>
	<li>Show a better error message if trying to play a Solarus 0.9 quest (#260).</li>
	<li>Remove built-in debug keys. This can be done from Lua now.</li>
	<li>Remove the preprocessor constant SOLARUS_DEBUG_KEYS.</li>
	<li>Call on_draw() before drawing menus.</li>
	<li>Fix .it musics looping when they should not.</li>
	<li>Log all errors in error.txt (#287).</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.1</h2>
<ul>
	<li>Add a GUI to <strong>automatically </strong><strong>upgrade quest files</strong> to the latest format (#247).</li>
	<li>Remove the initial prompt dialog to open a quest (#264).</li>
	<li>Replace non-free images by <strong>new icons</strong> (#245).</li>
	<li>Add <strong>tooltips</strong> to the add entity toolbar.</li>
	<li>Simplify the add entity toolbar by showing only one icon per entity type.</li>
	<li>Survive when images cannot be found (#256).</li>
	<li>Create more content when <strong>creating a new quest</strong> (#258, #279).</li>
	<li>Improve error messages.</li>
	<li>Fix a crash when creating a destructible without tileset selected (#283).</li>
	<li>Fix the sprite field disabled in the NPC properties dialog (#303).</li>
</ul>
<h2>Incompatibilities</h2>
These improvements involve changes that introduce some incompatibilities in both the format of data files (sprites, project_db.dat, languages.dat) and the Lua API (the dialog box and the game-over menu are scripted now!). Make a backup, and then see the <a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">migration guide</a> on the wiki to know how to upgrade.
<h2>Changes in Zelda Mystery of Solarus DX 1.7</h2>
New features:
<ul>
	<li>Add simplified Chinese and traditional Chinese translations (beta).</li>
	<li>Replace .spc musics by .it ones (much faster) (#17).</li>
	<li>Add an animated Solarus logo from Maxs (#57).</li>
</ul>
Bug fixes:
<ul>
	<li>Fix savegames created with Solarus 0.9 but that were never run.</li>
	<li>Fix a slight alignment issue with the hurt animation of the hero.</li>
	<li>Fix a small breach in dungeon 9 4F in the timed chest room (#4).</li>
	<li>Fix easy infinite rupees in the waterfall cave (#13).</li>
	<li>Fix low health beeping playing at final screen (#56).</li>
	<li>Add a teletransporter from south lake to the cave under the waterfall (#10).</li>
	<li>Dungeon 7 boss: the player could get stuck in the boss room (#6).</li>
	<li>Dungeon 7 boss: change the misleading hurt sound of the tail (#7).</li>
	<li>Dungeon 7: help the player be aligned correctly to obtain the boss key (#8).</li>
	<li>Dungeon 3 2F: fix two switches that disappeared when activating them.</li>
	<li>Dungeon 2 boss: fix tile disappearing issue (#14).</li>
	<li>Dungeon 9 2F: fix a minor graphical issue.</li>
</ul>
<h2>Changes in Zelda Mystery of Solarus XD 1.7</h2>
New features:
<ul>
	<li>Replace .spc musics by .it musics (faster) and remove unused ones.</li>
	<li>Add an animated Solarus logo from Maxs.</li>
</ul>
Bug fixes:
<ul>
	<li>Fix savegames created with Solarus 0.9 but that were never run.</li>
	<li>Fix a slight alignment issue with the hurt animation of the hero.</li>
	<li>Dungeon 1 3F: fix "attempt to compare number with nil" in counter 36 (#3).</li>
	<li>Dungeon 2 1F: fix an enemy not counting in WTF room.</li>
</ul>