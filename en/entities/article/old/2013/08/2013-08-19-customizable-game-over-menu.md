The last built-in menu is not built-in anymore in Solarus 1.1!

Now, when the life of the player reaches zero, an event game:on_game_over_started() is called. The game is suspended and you can show whatever you want. You can make a dialog that lets the player chose between "Continue" or "Save and quit". Or you can make a more elaborate game-over menu like in our games, where a fairy saves the player if he has one in a bottle.

And if you don't define the event, game:on_game_over_started(), then the engine just restarts the game.

The <a href="https://github.com/christopho/solarus/issues?milestone=2&amp;state=open">Solarus 1.1 roadmap</a> is close to its end. 50 issues were solved and 1 is still open. Now I am testing both games (ZSDX and ZSXD) to check everything because there was some refactoring done to upgrade to Solarus 1.1, and some C++ code was replaced by Lua code (most importantly, the dialog box and the game-over menu).