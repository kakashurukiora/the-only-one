<img class="aligncenter wp-image-1775 size-full" src="/images/gitlab-2.png" alt="" width="660" height="300" />

With the recent purchase of Github by Microsoft, the Solarus Team has decided to leave Github. We choose its alternative <strong>Gitlab</strong>, which we already know because we used it to secretly develop our last mini-game <a href="http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/">Mercuris Chess</a>, in April last year.

All the projects are still on Github for the moment, but in a read-only state. If you want to contribute, the code is now here on Gitlab : <a href="https://gitlab.com/solarus-games">https://gitlab.com/solarus-games</a>.

Be sure nothing will change, since Gitlab is almost identical. Development will work exactly as before.

See you soon!