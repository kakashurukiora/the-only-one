Solarus has now an official Twitter account. It is no more Christopho's one, even if both are obviously tightly linked. You can now follow us if you still don't.

<a href="https://twitter.com/SolarusGames"><img class="aligncenter wp-image-1207 size-medium" src="/images/follow_on_twitter-300x75.png" alt="follow_on_twitter" width="300" height="75" /></a>

And remember that we also have an official Facebook account, which by now will just forward the twits from its Twitter counterpart.

<a href="https://www.facebook.com/solarusgames"><img class="aligncenter wp-image-1206 size-medium" src="/images/follow_on_facebook-300x75.png" alt="follow_on_facebook" width="300" height="75" /></a>