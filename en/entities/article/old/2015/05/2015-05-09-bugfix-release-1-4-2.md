A bugfix version of Solarus was just released!

This is version <del>1.4.1</del> 1.4.2. It fixes some important problems including a crash with doors or chest using an equipment item as opening condition, and some problems in the quest editor when resizing maps or when copy-pasting entities. Thanks for your feedback!
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/engine/download/">Download Solarus 1.4</a></li>
	<li><a href="http://www.solarus-games.org/engine/solarus-quest-editor/">Solarus Quest Editor</a></li>
	<li><a title="LUA API documentation" href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
	<li><a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">Migration guide</a></li>
</ul>
And as always, here is the full changelog.
<h2>Changes in Solarus 1.4.2</h2>
<ul>
	<li>Fix crash with doors whose opening condition is an item (#686).</li>
	<li>Fix the size of custom entities supposed to be optional (#680).</li>
	<li>Fix the hero's sprite reset to default ones when changing equipment (#681).</li>
	<li>Fix animated tiles freezed when running a quest a second time (#679).</li>
	<li>Fix saving empty files.</li>
	<li>Print an error message when there is no font in the quest.</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.4.2</h2>
<ul>
	<li>Fix crash when copy-pasting both tiles and dynamic entities sometimes (#24).</li>
	<li>Fix crash when creating a map when no tileset exists.</li>
	<li>Fix the view not updated when resizing a map (#25).</li>
	<li>Fix maps having having an initial size of zero.</li>
	<li>Fix inversion between generalized and usual NPCs in the entity dialog.</li>
	<li>Fix the choice of the layer when copy-pasting entities.</li>
	<li>Fix a precision issue when adding or moving entities.</li>
	<li>Fix entities remaining visible when adding them on a hidden layer.</li>
	<li>Fix the entity dialog allowing special characters in the entity name.</li>
	<li>Fix missing translation of special destination names.</li>
	<li>Fix closing a file even if unsaved when renaming it.</li>
	<li>Fix typos.</li>
	<li>Sprite editor: fix adding a new direction with the plus button (#27).</li>
</ul>
Enjoy!