# {title}

[youtube id="bXQ7WGK8kvg"]

## Summary

- Creating a bridge
  - Using layers to allow the hero to walk on or below the bridge
  - Invisible platform trick to prevent the hero to fall too soon to the floor below

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
