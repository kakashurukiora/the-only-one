# {title}

[youtube id="RvV2rU75WmA"]

## Summary

- Configuring the HUD scripts (menus)
  - Hearts
  - Rupees
  - Item
  - `attack_icon` & `action_icon`

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)

You can reuse in your projects the HUD elements from our games:

- [Zelda ROTH SE](https://gitlab.com/solarus-games/zelda-roth-se) or [Zelda OLB SE](https://gitlab.com/solarus-games/zelda-olb-se): HUD scripts in the style of *A Link to the Past*.
- [Zelda MOS DX](https://gitlab.com/solarus-games/zsdx) or [Zelda Mercuris' Chest](https://gitlab.com/solarus-games/zelda-mercuris-chest) : HUD scripts in the style of *Ocarina of Time*.
