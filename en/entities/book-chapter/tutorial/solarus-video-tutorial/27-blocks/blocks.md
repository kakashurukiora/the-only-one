# {title}

[youtube id="pOm_I2kG6vM"]

## Summary

- Working with block entities
  - Pushing only once
  - Unlimited pushes
  - Also allow pulling
  - Restricting movement direction
  - Pushing a block onto a switch only activated by block
- Using blocks in the map script
  - Making chest appear when switch activated and hide again when deactivated
  - Locking the switch after chest is opened

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Useful resources to reproduce this chapter](https://www.solarus-games.org/tuto/en/basics/ep16_resources.zip)
