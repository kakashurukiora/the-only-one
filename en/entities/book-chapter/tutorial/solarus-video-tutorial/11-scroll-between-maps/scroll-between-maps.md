# {title}

[youtube id="5SN2RKV09ec"]

## Summary

- Using scrolling teletransporters on maps of different sizes
  - Setting the world location values
  - How to fix problems with camera scrolling

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
