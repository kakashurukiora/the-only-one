### Presentation

*The Legend of Zelda: The Minish Cap*, released in 2004 on Game Boy Advance, is the last 2D Zelda episode. It features gorgeous pixel art, and a rich gameplay, with elements taken from 3D episodes and other totally new, including the ability to shrink to the size of a mouse! Despite its main quest's short length, the game's strength lies in its charming and highly influential art direction.

This resource pack has been initiated by [Gregory McGregerson](https://github.com/GregoryMcGregerson) but is currently in need of artists and developers to be completed. Join us on [the forum](http://forum.solarus-games.org/index.php/topic,1346.0.html) to help!

![The Legend of Zelda: The Minish Cap box](images/box_art.png)
