### Presentation

Inspired by the pervasive question *“what would vegans do if stranded on a desert island?”* Vegan on a Desert Island provides an answer: **they’d try to fix it.**

![Rachel](artworks/artwork_rachel.png)

In this 2D puzzle adventure, a hypothetical question is taken to the extreme. Packed with cheeky political dialogue, puzzles, and a meaningful story, Vegan on a Desert Island is an artful union between retro-game aesthetics and the absurdity of postmodern culture.

![Rachel](artworks/artwork_seagulls.png)

Real-world animal issues such as human pollution, wild animal predation, and species discrimination are explored in the game’s themes. Vegan on a Desert Island is a meaningful title that promises to make you think.

### Synopsis

Dissatisfied with the banality of her life, Rachel agrees to elope with Marvin to a tropical Pacific island. While trying to park the ship, the duo smash into a cliff and wash ashore. Marvin dies, and Rachel is left to venture the island alone.

![Greybeard](artworks/artwork_greybeard.png)

As a vegan, Rachel can speak to the island’s animal inhabitants. Their communities are rife with conflict that only an outsider like Rachel can fix. She must make connections with villagers, explore the island, and solve puzzles that stand in her way. Combating issues like human-caused pollution and wild animal suffering, Rachel is hopeful she can fix the world.

But working with others isn’t so easy. Can she really fix the island's messy problems?

*Illustrations by [theArtofSilent](https://www.instagram.com/theartofsilent/). Licensed under CC BY-SA 4.0.*
