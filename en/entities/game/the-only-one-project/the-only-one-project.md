## Description

**The Only One Project** is a small hardcore adventure with a pre-dungeon, and a big dungeon with lots of twisted puzzles, a "funny" mini-boss, and a big final boss!

## Synopsis

**Link** wakes up at home, in a world parallel to A Link to the Past!
He finds himself without a weapon, with nothing! Out of his house, he goes in search of information, to understand what is going on!

He will discover that a thick dark haze covers the world, and that something strange is happening in a nearby castle.
The guards blocking access to the castle, **Link** will have to find another way out to be able to infiltrate it!

### Main quest and side quests

For players less comfortable with 2D Zelda, hard / harcore modes, and twisted puzzles, this could take you between 1 to 2 hours.