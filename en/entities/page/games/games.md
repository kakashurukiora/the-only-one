[container]

# {title}

[row]
[column width="3"]
[/column]
[column width="9"]
[entity-search text-placeholder="Search for"]
[/column]
[/row]

[space]

[row]

<!--Filters-->
[column width="3"]
[box-highlight]
[entity-filter entity-type="game" filter="types" title="Genre"]
[entity-filter entity-type="game" filter="languages" title="Language"]
[entity-filter entity-type="game" filter="states" title="State"]
[/box-highlight]
[/column]

<!--Games -->
[column width="9"]
[game-listing orderby="title"]
[/column]

[/row]

[/container]