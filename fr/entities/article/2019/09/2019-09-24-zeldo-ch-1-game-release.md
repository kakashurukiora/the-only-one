Il y a deux ans, en 2017, ZeldoRetro avait créé son premier jeu avec Solarus : le premier chapitre de sa série *Le Défi de Zeldo*, intitulé *La Revanche du Bingo*. Mais à l'époque, c'était seulement un petit défi destiné à son ami Adenothe, et pas destiné à être publié publiquement. Cependant, la sortie du second chapitre l'a fait reconsidérer cette décision, car tout le monde se demandait "Attendez ? Mais si ceci est le chapitre 2, où est donc le chapitre 1 ?". Le voici !

![Logo](2019-09-24-zeldo-ch-1-game-release/game-logo.png)

Le jeu était bien moins ambitieux que sa suite. Il contient seulement un donjon, et pas de monde à explorer, ce qui le rend plutôt court. Il se finit en moins d'une heure.

![Screenshot 1](2019-09-24-zeldo-ch-1-game-release/screen_1.png)

![Screenshot 2](2019-09-24-zeldo-ch-1-game-release/screen_2.png) 

Vous pouvez télécharger le jeu sur [sa page dans la Bibliothèque de Quêtes Solarus](/fr/games/defi-de-zeldo-ch-1).

[![Téléchargement](2019-09-24-zeldo-ch-1-game-release/game-download.png)](/fr/games/defi-de-zeldo-ch-1)
