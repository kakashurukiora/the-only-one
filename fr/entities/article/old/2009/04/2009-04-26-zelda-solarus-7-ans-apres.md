Le 26 avril 2002, il y a 7 ans, c'était à peu près à cette heure ci que sortait, après un an et demi d'attente, notre première création, [url=http://www.zelda-solarus.com/jeu-zs]Zelda: Mystery of Solarus[/url]. A cette époque, sans l'ADSL, il m'avait fallu 45 minutes pour envoyer les 5.5 Mo du jeu sur le serveur... Il avait même fallu le faire deux fois car la première version ne fonctionnait pas :P.
Sept ans et 360 000 téléchargements plus tard, nous sommes fiers d'être toujours là et de continuer à créer des jeux :). Ne pouvons que vous remercier pour votre fidélité ^^.

Pour fêter ça, nous vous proposons aujourd'hui un fond d'écran dédié à Zelda Solarus DX, et réalisé par Neovyze, dessinateur de talent qui réalise aussi des artworks pour le jeu :

[center][url=http://www.zelda-solarus.com/jeu-zsdx-wallpapers][img]http://www.zelda-solarus.com/images/zsdx/wallpapers/wallpaper_mini.jpg[/img][/url][/center]

Ce fond d'écran contient une nouvelle version du logo du jeu qui représente l'Amulette du Solarus, un objet capital dans le scénario ;). A très bientôt pour d'autres surprises ^^.