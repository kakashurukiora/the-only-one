Vous l'avez peut-être remarqué : un compteur a été ajouté sur la page d'accueil, indiquant le nombre de jours restants avant la sortie de la démo de notre projet [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url]. Or, nous sommes aujourd'hui le 18 novembre, il ne reste qu'un mois avant la sortie de la démo, fixée rappelons-le au [b]vendredi 18 décembre[/b], le jour des vacances de Noël !

La démo est en fait déjà terminée, le travail qui reste consiste à la préparer pour les différentes plates-formes (Windows, Linux, Mac OS X...) et à faire les différentes traductions. Je ne sais pas encore quelles langues seront prêtes dès le 18 décembre, mais il y aura au moins le français (heureusement :D) et l'anglais. Les tests semblent toucher à leur fin car on trouve de moins en moins de bugs. Bien sûr, on trouve quand même des idées de choses à améliorer. Certaines d'entre elles pourront attendre le jeu complet, d'autres ont été prises en compte dans la démo. Par exemple, Neovyse a refait l'écran-titre. C'est la troisième version et elle fait l'unanimité parmi les testeurs ;). En voici une capture d'écran :
[center]
[img]http://www.zelda-solarus.com/images/zsdx/title_screen_v3.png[/img]
[/center]
Ce nouvel écran-titre est plus évolué car le fond s'anime, contrairement à celui que vous pouviez voir dans la [url=http://www.youtube.com/watch?v=r3e7t0QvImY]vidéo[/url]. Et peut-être que selon les conditions dans lesquelles vous lancez le jeu, vous aurez des surprises, qui sait ?

Pendant ce mois restant, en parallèle de la finalisation de la démo, je travaille déjà sur le jeu complet. Certaines maps ont été commencées et je vous en parlerai dans une prochaine news :).
Je vous laisse sur deux autres captures d'écran, dont une de l'intro qui n'avait pas encore été dévoilée jusqu'à présent...
[center]
[img]http://www.zelda-solarus.com/images/zsdx/intro.png[/img] [img]http://www.zelda-solarus.com/images/zsdx/fairy_cave.png[/img] 
[/center]