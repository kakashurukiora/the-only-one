En quelques jours, le développement de notre projet Zelda : Mystery of Solarus DX a beaucoup avancé.
Avant tout, voici l'entrée du premier donjon (celui de la future démo), qui a gagné en allure :) :
[center]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon1_outside.png[/img][/center]
A l'intérieur, toutes les salles du premier donjon sont faites. Pour être plus précis, tous les décors ont été posés, ainsi que les ennemis, les coffres et la plupart des éléments. Il reste donc à les programmer, c'est-à-dire écrire les scripts qui vont définir les énigmes, ce qui se passe lorsqu'on appuie sur un interrupteur, l'apparition des coffres, l'ouverture des portes... Pour l'instant, si vous aviez la possibilité de jouer, vous pourriez traverser toutes les pièces sans rien faire :P.
[center]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon1_3.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon1_1.png[/img]
[/center]
Les salles sont bien sûr reproduites d'après Zelda : Mystery of Solarus premier du nom, avec cependant beaucoup d'améliorations architecturales afin de donner un aspect plus esthétique et une meilleure cohérence. C'est notamment le cas du dernier étage de ce donjon qui a été entièrement repensé.

Les labyrinthes et les énigmes sont repris et adaptés. Les labyrinthes que je juge les moins intéressants sont remplacés par des phases de combats, séquences qui prennent tout leur intérêt dans cette version DX où les combats sont beaucoup plus agréables à jouer. Il y aura donc plus de combats, sans diminuer le nombre d'énigmes puisque seuls les labyrinthes rébarbatifs seront enlevés. Au contraire, vous verrez que les énigmes sont plus intéressantes et plus variées, en tout cas je fais le maximum pour :)

Prochaines étapes : programmer les événements du donjon, ce qui nécessitera de compléter deux petites choses que le moteur ne gère pas encore : les ouvertures et fermetures de portes (verrouillées ou non) et les tapis roulants. Puis viendra la création du boss et du mini-boss, qui donneront lieu à des news prometteuses :P.
Bref, une fois que tout ceci sera fait, on commencera à y voir plus clair concernant la date de sortie de la démo, toujours annoncée pour 2009 rappelons-le :).
[center]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon1_2.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon1_4.png[/img]
[/center]
[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-images]Galerie d'images[/url][/li]
[/list]