La soluce vidéo de [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url] continue sur sa lancée ! Ce soir je viens d'enregistrer deux nouveaux épisodes avec Morwenn.

[list]
[li] [url=http://www.youtube.com/watch?v=abOn5rruZ3E]Épisode 7 : du niveau 3 au niveau 4[/url][/li]
[li] [url=http://www.youtube.com/watch?v=PuBk5Z7JEJ8]Épisode 8 : niveau 4 - Palais de Beaumont[/url][/li]
[/list]

Nous en sommes à la fin du quatrième donjon. Bientôt la suite !