La fin du jeu approche !

Nous continuons la solution en vidéo avec trois nouveaux épisodes : le donjon secret, du niveau 6 au niveau 7, et le niveau 7. Merci à Binbin et BenObiWan pour les commentaires :)

[list]
[li][url=http://www.youtube.com/watch?v=0mQrcTSgTt8]Épisode 13 : Donjon secret[/url][/li]
[li][url=http://www.youtube.com/watch?v=VhKdF1L7Vak]Épisode 14 : Du niveau 6 au niveau 7[/url][/li]
[li][url=http://www.youtube.com/watch?v=Q1hjSJMHr0Y]Épisode 15 : Temple du Cristal[/url][/li]
[/list]

Enjoy ! ^_^