<p>Comme vous avez pu le constater, le site était indisponible pendant une partie de la journée pour cause de maintenance. En effet, nous sommes hébergés sur un serveur mutualisé, c'est-à-dire que nous partageons le même serveur avec d'autres sites. Autrement dit, chaque site ne doit pas utiliser trop de ressources sinon il finirait par faire planter le serveur et tous les sites qui sont dessus.</p>

<p>C'est la raison pour laquelle nous avons reçu un avertissement de notre hébergeur, nous signalant que nous dépassions la limite de 3600 hits par heure. Bref, tout ça pour vous dire que nous avons dû optimiser le code source du site afin d'ouvrir moins de fichiers à chaque chargement d'une page. Ces changements ne sont cependant pas visibles pour vous car nous tenions à conserver le même design.</p>

<p>De plus nous allons déménager les forums très prochainement vers un autre hébergeur afin de réduire le nombre de hits. Par conséquent ils sont actuellement encore fermés mais on peut espérer qu'ils seront à nouveau disponibles demain.</p>

<p>Voilà, nous vous présentons toutes nos excuses pour ces incidents indépendant de notre volonté :-)</p>