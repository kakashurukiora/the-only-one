<p>Nous vous le promettions depuis longtemps, et il est enfin là ! Admirez le premier artwork de Link pour Zelda M'C, dessiné par Neovyze :</p>

<p align="center"><a href="../data/fr/entities/article/old/2005/02/images/link1.jpg" target="_blank"><img src="/images/link1_mini.jpg" width="176" height="298" border="0"></a></p>

<p>Nous souhaitons connaître votre avis sur le style de Link ! Alors n'hésitez pas à réagir en postant vos commentaires et en répondant au sondage sur la droite de la page :)</p>