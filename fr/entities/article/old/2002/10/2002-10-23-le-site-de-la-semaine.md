<p>Maxime Barthélémy, un fidèle visiteur du site, a réussi à faire élire Zelda Solarus site de la semaine sur jeuxvideo.com ! Nous ne le remercierons jamais assez pour ce gros coup de pub.</p>

<p>Zelda Solarus devient de plus en plus connu et de plus en plus visité. La popularité du site n'a cessé de croître depuis sa création et elle augmente plus que jamais en ce moment. Merci à vous tous !</p>