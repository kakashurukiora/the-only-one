<p>Vous savez peut-être (ou peut-être pas) que notre création Zelda : Mystery of Solarus, bien avant d'être informatisée, existait déjà presque entièrement sur papier. Et oui car c'est un projet qui date de 1995 !</p>

<p>Pour la première fois, nous vous proposons des images de cette version papier.</p>

<p align="center"><a href="../data/fr/entities/article/old/2004/11/images/niveau7.jpg" target="_blank"><img src="/images/niveau7_mini.jpg" border="0" width="300" height="229"></a></p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=versionpapier">Voir les images de la version papier</a></p>