Je vois que ce sournois de Christopho a réussi à donner signe de vie... Qu'à cela ne tienne, il ne risque plus de recommencer désormais !

En effet, c'est moi qui ai volé tous les secrets de Mercuris' Chest pour les revendre à Sony. C'était le seul moyen pour moi de leur prouver de quel côté j'étais.

J'ai dû me plonger dans cette longue période d'infiltration et il a également fallu que je brouille toutes les pistes notamment en ce qui concerne mon alter-ego Metallizer. Quelle autre meilleure idée aurais-je pu avoir que d'inventer ces soit-disant échanges houleux directement sur le forum ? Tout le monde n'y a vu que du feu ! Christopho en premier !

Cinq années... Cinq années de ma vie ont été sacrifiées mais aujourd'hui, je serai enfin récompensé !

L'heure approche mes amis. Dans très peu de temps maintenant, le monde plongera de manière irrévocable dans le Sonysme !