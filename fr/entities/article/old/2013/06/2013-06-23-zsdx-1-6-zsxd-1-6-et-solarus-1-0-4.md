<a href="../data/fr/entities/article/old/2013/06/images/solarus-logo-black-on-transparent.png"><img class="aligncenter" alt="Logo du moteur Solarus" src="/images/solarus-logo-black-on-transparent-300x90.png" width="300" height="90" /></a>

<strong>EDIT</strong> : Des instabilités ont été signalées dans les versions 1.6.0 et 1.6.1, en particulier dans Zelda Mystery of Solarus XD. Une nouvelle version 1.6.2 corrige les problèmes connus. N'hésitez pas à continuer à rapporter les éventuels bugs sur le forum !

Des nouvelles versions (1.6) de Zelda Mystery of Solarus DX et Zelda Mystery of Solarus XD viennent de sortir ! Nos deux jeux sont maintenant entièrement fonctionnels avec le moteur Solarus 1.0, alors que jusqu'à présent, ils fonctionnaient encore avec l'ancien moteur (0.9). Cela ne change pas énormément de choses pour les joueurs, mais les personnes qui s'intéressent à la création de jeux ont maintenant deux exemples de jeux complets utilisant notre nouveau moteur de jeu. Et la possibilité d'en réutiliser des parties pour créer leurs propres jeux, comme le menu de pause ou le HUD.

Passer nos deux jeux à Solarus 1.0 à été un long travail, mais cela a permis d'améliorer énormément les fonctionnalités du moteur, ce qui rend directement service à tous les futurs jeux créés par nous ou par d'autres !

Le moteur a d'ailleurs lui aussi été mis à jour en version 1.0.3, avec au programme de nombreuses corrections de bugs et améliorations. Il y a notamment des corrections de crashs possibles et une amélioration de la gestion du plein écran sur Mac OS X (merci Vlag67 et Lelinuxien). Je vous recommande de passer à cette nouvelle version si vous utilisez Solarus pour développer un jeu. Des bugs ont également été corrigés dans les deux jeux, notamment un glitch qui permettait aux joueurs malins d'atteindre le donjon 8 de Zelda Mystery of Solarus DX avant d'avoir commencé le 4 ! Et c'était possible dans toutes les versions, de 1.0 à la plus récente 1.5.2. Comment pouvait-on faire ? Je vous laisse chercher :P

Pour l'occasion, Zelda Mystery of Solarus DX se dote aussi d'un tout nouvel écran-titre grâce à Neovyse !
<h2>Nouvelles versions de nos jeux</h2>
Le format des fichiers de sauvegarde a changé avec Solarus 1.0, mais ne vous en faites pas : vos sauvegardes seront automatiquement converties. Il n'y a donc pas de risque de perdre vos données.
<ul>
	<li><span style="line-height: 13px;">Télécharger <a href="http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/">Zelda Mystery of Solarus DX 1.6.2</a></span></li>
	<li>Télécharger <a href="http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/">Zelda Mystery of Solarus XD 1.6.2</a></li>
</ul>
Malgré nos tests, il n'est pas impossible que de nouveaux bugs nous aient échappés. En effet, une grande partie de la programmation des deux jeux a dû être réécrite pour fonctionner avec le moteur Solarus 1.0. N'hésitez pas à nous signaler tout problème sur le <a href="http://forums.zelda-solarus.com/index.php/board,62.0.html">forum</a> !
<h2>Nouvelle version du moteur</h2>
Il s'agit d'une version de corrections de bugs (1.0.1 vers 1.0.4), c'est-à-dire que si vous développez un jeu avec Solarus 1.0.x, tout reste entièrement compatible.
<ul>
	<li>Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.0.4-win32.zip">Solarus 1.0.4 + l'éditeur de quêtes</a> pour Windows</li>
	<li>Télécharger <a href="http://www.solarus-games.org/downloads/solarus/macosx/solarus-1.0.4-macosx64.zip">Solarus 1.0.4 + l'éditeur de quêtes</a> pour Mac OS X 10.6+</li>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.0.4-src.tar.gz">code source</a></li>
</ul>
Pour plus de détails sur les changements, vous pouvez visiter notre <a href="http://www.solarus-games.org/2013/06/22/solarus-1-0-2-released-zsdx-and-zsxd-upgraded-to-the-new-engine/">blog de développement</a> (en anglais).

PS : si vous avez téléchargé ZSDX ou ZSXD 1.6.0 ou 1.6.1, des bugs importants viennent d'être corrigés. Les blocs ne pouvaient plus être poussés correctement. Il y avait également deux crashs dans ZSXD. Enfin, la personnalisation des commandes du joypad ne fonctionnait plus. Il vous faut télécharger ZSDX ou ZSXD 1.6.2 afin d'avoir les correctifs.