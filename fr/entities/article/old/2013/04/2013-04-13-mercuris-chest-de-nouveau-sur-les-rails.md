Dix ans qu'on vous parle de ce projet. Il y a eu une démo, il a été recommencé, annulé et maintenant il redémarre. Comment présenter Zelda Mercuris' Chest en quelques mots ?

<a href="/images/tannek.jpg"><img class="wp-image-9744 alignleft" alt="Tannek" src="/images/tannek.jpg" width="212" height="360" /></a>

<a href="http://www.zelda-solarus.com/zs//jeu/zelda-mercuris-chest/">Zelda Mercuris' Chest</a> (en français, le coffre de Mercuris :P) est le successeur de <a href="http://www.zelda-solarus.com/zs//jeu/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus</a> (maintenant DX). Mais ce n'est pas une suite au point de vue du scénario. L'histoire se déroulera bien à part. À vrai dire, une grande partie du scénario avait déjà été écrite lors de la première vie de ce projet, mais on vous a pas dévoilé grand chose si ce n'est quelques <a href="http://www.zelda-solarus.com/zs/article/zmc-images/">artworks</a>.

La <a href="http://www.zelda-solarus.com/zs/article/zmc-telechargements/">démo</a>, à laquelle on vous conseille de jouer, comporte un immense donjon dont le but était de servir de mise en bouche pour le jeu complet. Il n'y a pas de scénario : le seul but est de terminer le donjon ! Ce donjon, intitulé le Temple du Rail, avait pour thème les wagonnets et les rails. Un parcours semé d'embûches doit être traversé pour vous mener, vous et vos wagonnets, jusqu'à la bataille épique contre Colossius, le boss du donjon.

<a href="../data/fr/entities/article/old/2013/04/images/zap09.png"><img class="aligncenter size-medium wp-image-9779" alt="zap09" src="/images/zap09-300x225.png" width="300" height="225" /></a>

(Rappelons d'ailleurs que ce temple tout entier fait un clin d'oeil à Perfect Dark, célèbre jeu de tir subjectif de la Nintendo 64, en reprenant exactement les mêmes salles que le niveau Secteur 51 !)

Le Temple du Rail sera inclus dans le jeu complet, je peux vous l'annoncer ! Et sous une autre forme remasterisée : il sera situé au fond d'une ancienne mine, un endroit qui aura son importance dans le scénario. Ces captures d'écrans viennent donc du nouveau Temple du Rail :
<p style="text-align: center;"><a href="../data/fr/entities/article/old/2013/04/images/zmc_mine_1.png"><img class="size-medium wp-image-32589" alt="" src="/images/zmc_mine_1-300x225.png" width="300" height="225" /></a> <a href="../data/fr/entities/article/old/2013/04/images/zmc_mine_2.png"><img class="size-medium wp-image-32590" alt="" src="/images/zmc_mine_2-300x225.png" width="300" height="225" /></a></p>
Cette version remasterisée du Temple du Rail est dotée de graphismes inédits que nous avons créés avec amour, spécialement pour faire un style de mine abandonnée. Au niveau de l'avancement, les maps du donjon sont en cours de reproduction avec l'éditeur de map. Mymy m'aide beaucoup pour cette tâche ; merci aussi à Angenoir37 et Miniriël qui ont un peu travaillé dessus en décembre.  La reproduction des salles à l'identique est presque terminée. Ensuite je passerai à la partie programmation pour que le donjon devienne jouable. Les testeurs pourront donc bientôt essayer :)

En parallèle, d'autres parties du jeu avancent, mais ce sera pour une autre mise à jour ! À très bientôt pour d'autres nouvelles.
<p style="text-align: center;"></p>