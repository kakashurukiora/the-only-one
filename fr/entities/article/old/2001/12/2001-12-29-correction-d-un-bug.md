<p>On vient de nous signaler qu'il y avait un gros  bug sur la page de téléchargements. En effet, lorsque vous vouliez télécharger la démo, les RTP ou les polices d'écran françaises, vous arriviez sur la page de téléchargement mais celle-ci se fermait tout de suite et renvoyait à la page d'accueil, au lieu de démarrer le téléchargement.</p>

<p>Ce bug, dû au PHP et au nouveau design du site, est désormais corrigé. Vous pouvez donc télécharger sans problème :</p>

<ul type=disc>
<li><a href="http://www.zelda-solarus.com/download.php3?name=demo">La démo</a> (1 Mo)</li>
<li><a href="http://www.zelda-solarus.com/download.php3?name=RTP">Les RTP</a> (11 Mo)</li>
<li><a href="http://www.zelda-solarus.com/download.php3?name=polices_icone">Les polices d'écran françaises (avec les accents)</a></li>
</ul>

<p>Pour plus de détails sur les téléchargements, rendez-vous sur la page réservée à la <a href="http://www.zelda-solarus.com/demo.php3">demo</a>.</p>