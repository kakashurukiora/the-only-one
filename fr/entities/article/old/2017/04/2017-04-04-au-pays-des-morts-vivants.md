XD2 ou non, le screenshot de la semaine continue concernant Zelda Mercuris Ches<strong>t</strong>.

Aujourd'hui on vous présente un travail en cours par l'inévitable Newlink dans : le cimetière !

<a href="../data/fr/entities/article/old/2017/04/images/stalkis.png"><img class="aligncenter size-medium wp-image-38140" src="/images/stalkis-300x225.png" alt="stalkis" width="300" height="225" /></a>

On ne sait pas encore très bien ce qu'il y aura comme quêtes à remplir dans ces lieux, mais ces squelettes inédits ne vous rendront pas la tâche facile !