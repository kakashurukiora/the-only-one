## Licence du moteur et des quêtes Solarus

Le moteur Solarus est sous licence **GNU GPL v3**. De nombreuses quêtes réalisées avec le moteur sont sous la même licence, mais cela ne signifie pas que toutes les quêtes doivent l'être. Dans les faits, vous pouvez placer votre quête sous la licence que vous souhaitez, en échange de quoi vous devez respecter certaines, dépendantes de votre choix de licence. Cela signifie que OUI, vous pouvez réaliser un jeu commercial sous licence propriétaire avec Solarus.

Un [chapitre à propos de ces questions de licence](/en/development/tutorials/solarus-official-guide/basics/choosing-a-license) a été ajouté au tutoriel en ligne, et explique les choses en détail.

![Licence de Solarus et des quêtes Solarus](images/solarus-licensing.png)

## Le Solarus MIT Starter Pack, compatible avec un jeu commercial et propriétaire

Coïncidence, la communauté Solarus a initié un projet appelé [MIT Starter Quest](/fr/development/resource-packs/mit-starter-quest). Ce pack de ressource ne contient que des scripts sous licence MIT et des ressources en CC0 (aussi appelé domaine public). Cela signifie que vous pouvez utiliser ces scripts et ressources pour des projets propriétaires et commerciaux. Quelques scripts essentiels comme celui des multi-events ou de la boite de dialogue ont été réécrits et placés sous licence MIT.

[![MIT Starter Quest thumbnail](images/mit-starter-quest-thumbnail.png)](/fr/development/resource-packs/mit-starter-quest)
