# {title}

[youtube id="rte8Yd5OFvk"]

## Sommaire

- Différence entre les PNJ habituels (personnages) et les PNJ généralisés (objets)
- Créer une pancarte qui affiche un dialogue
- Interagir avec un PNJ derrière un comptoir
  - Utiliser un script de map pour changer la direction du PNJ lors de l'interaction

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Ressources utiles pour reproduire ce chapitre](https://www.solarus-games.org/tuto/fr/basics/ep19_ressources.zip)
