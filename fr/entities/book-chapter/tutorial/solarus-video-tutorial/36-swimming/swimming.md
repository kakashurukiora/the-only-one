# {title}

[youtube id="KzblUhVng3s"]

## Sommaire

- Créer l'objet palmes
- Créer le dialogue pour les palmes
- Utiliser la capacité `swim`
- Faire sauter le héros dans l'eau profonde (la capacité `jump_over_water`)

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
