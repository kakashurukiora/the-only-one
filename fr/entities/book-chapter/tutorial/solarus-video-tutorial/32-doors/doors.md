# {title}

[youtube id="0BLePicALSo"]

## Sommaire

- Utiliser les entités portes
  - Ouvrir les portes quand un interrupteur est activé
  - Ouvrir les portes quand le héros interagit avec elles (et sauvegarder cet état)

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
