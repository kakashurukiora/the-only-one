### Présentation

*The Legend of Zelda: Mystery of Solarus XD* est un jeu parodique que nous avons sorti le 1er avril 2011. Bien que ce soit un gros poisson d'avril, c'est aussi un vrai jeu, comprenant deux immenses donjons et entre 5 et 10 heures de jeu.

Il a été développé en une période de temps très courte (sept semaines), grâce à du café, de la bière et des pizzas. Malgré le défi qu'à été son développement en temps limité, le jeu est complet, avec de nombreuses blagues, références, et PNJs excentriques.

![Link](artworks/artwork_link.png "Link")

### Synopsis

Comme dans de nombreux jeux de la série *The Legend of Zelda*, votre aventure commence par le réveil de Link. Cependant, aujourd'hui, Link se réveille amnésique, sans mémoire de la veille. Ce n'est pas tout : son épée et son bouclier ont disparu, et plus important, il a perdu trace de la princesse Zelda ! Que s'est-il passé ? Où est-elle ? Que lui est-il arrivé ?

C'est ainsi que débute votre aventure... à la recherche de la princesse Zelda, de votre équipement et de vos souvenirs.

![Zelda](artworks/artwork_zelda.png "Zelda")
