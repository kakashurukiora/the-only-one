### Télécharger Solarus

Pour ouvrir un jeu Solarus, vous avez besoin de l'application **Solarus Launcher**. Assurez-vous d'abord de l'avoir [installée](/fr/solarus/download).

Si vous avez déjà Solarus Launcher, vous pouvez passer à l'**étape 2**.

### Télécharger le jeu

Cliquez sur le bouton suivant pour télécharger la quête sous forme de **fichier .solarus**. Vous pouvez alors l'ouvrir avec l'application **Solarus Launcher**.
